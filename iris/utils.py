def train_test_split(data_frame, X_features, y_features, frag=0.8 ):
    train = data_frame.sample(frac=frag, random_state=2021)
    test = data_frame.drop(train.index)
    X_train = train.loc[:,X_features]
    X_test = test.loc[:,X_features]

    y_train = train[y_features]
    y_test = test[y_features]

    return X_test, X_train, y_test, y_train

#call function
#train_test_split(laptops, ["CPU","CPU"], "price_euros")
