import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow.keras as keras
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

from sklearn import preprocessing  # change!! https://pbpython.com/categorical-encoding.html
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau

label_encoder = preprocessing.LabelEncoder()

iris_df = pd.read_csv('iris.csv')

x_features = ['sepal_length', 'sepal_width', 'petal_length', 'petal_width']
y_feature = 'species'

# labelencodel
# iris_df['species']= label_encoder.fit_transform(iris_df['species'])
iris_df['species'] = iris_df['species'].astype('category')
iris_df['species'] = iris_df['species'].cat.codes

print(iris_df['species'].unique())

X = iris_df.loc[:, x_features]
y = iris_df[y_feature]

X_train = X.sample(frac=0.8, random_state=2021)
X_test = X.drop(X_train.index)

y_train = y[X_train.index]
y_test = y[X_test.index]

normalizer = keras.layers.experimental.preprocessing.Normalization()
# Si X sols té una característica
# normalizer = keras.layers.experimental.preprocessing.Normalization(input_shape=[1,])
normalizer.adapt(np.array(X_train))

model = keras.models.Sequential()
# Capa d'entrada
model.add(normalizer)

# Capes ocultes
#model.add(tf.keras.layers.Dense(8, activation=tf.nn.relu))
model.add(tf.keras.layers.Dropout(0.2))
model.add(tf.keras.layers.Dense(8, activation=tf.nn.relu))
#model.add(tf.keras.layers.Dense(120, activation=tf.nn.relu))

# Capa de sortida
model.add(tf.keras.layers.Dense(3, activation=tf.nn.softmax))  # Classificació

model.compile(optimizer=tf.optimizers.Adam(learning_rate=1e-4),
              loss="sparse_categorical_crossentropy",
              metrics=['accuracy'])


# Mètode per imprimir una gràfica amb l'error del entrenament
def plot_loss(history):
    plt.plot(history.history['loss'], label='loss')
    if history.history.get('val_loss'):
        plt.plot(history.history['val_loss'], label='val_loss')
        plt.yscale('log')
        plt.xlabel('Epoch')
        plt.ylabel('Error')
        plt.legend()
        plt.grid(True)
        plt.show()

early_stopping_cb = keras.callbacks.EarlyStopping(patience=5)
reduce_lr_cb = ReduceLROnPlateau(patience=3, min_lr=0.001, mode='max')
history = model.fit(X_train, y_train, epochs=100000, validation_split=0.2, callbacks=[reduce_lr_cb, early_stopping_cb])
plot_loss(history)

# # probar el modelo
val_acc, val_los = model.evaluate(X_test, y_test)

print('val_acc %2.4f' % val_acc)
print('val_los %2.4f' % val_los)



# crear
# fit
# score
