import re
import matplotlib.pyplot as plt

def extract_ram(x):
    match = re.search(r"(\d+)GB", x)
    if match:
        # print(match.groups())
        return int(match.groups()[0])
    else:
        raise Exception("Error al extreure la RAM", x)


def extract_weigth(x):
    match = re.search(r"(\d+(\.\d+)?)", x)
    if match:
        # print(match.groups()[0])
        return float(match.groups()[0])
    else:
        raise Exception("Error al extreure la Weigth", x)


def extract_cpu(x):
    match = re.search(r"(\d+(\.\d+)?)GHz", x)
    if match:
        # print(match.groups()[0])
        return float(match.groups()[0])
    else:
        raise Exception("Error al extreure la CPU", x)


def extract_screen(x):
    # match = re.search(r"(\d+(x\d+))", x)
    match = re.search(r"(\d+)", x)
    if match:
        # print(match.groups()[0])
        return int(match.groups()[0])
    else:
        raise Exception("Error al extreure Screen Resolution", x)


def plot_loss(history):
    plt.plot(history.history['loss'], label='loss')
    if history.history.get('val_loss'):
        plt.plot(history.history['val_loss'], label='val_loss')
    plt.xlabel('Epoch')
    plt.ylabel('Error')
    plt.legend()
    plt.grid(True)
    plt.show()
