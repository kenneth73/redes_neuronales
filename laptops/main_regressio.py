import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow.keras as keras
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

laptops = pd.read_csv("laptops.csv", encoding="utf-16")
print(laptops)

import utils as utils

laptops["Cpu"] = laptops["Cpu"].apply(utils.extract_cpu)
laptops["Ram"] = laptops["Ram"].apply(utils.extract_ram)
laptops["ScreenResolution"] = laptops["ScreenResolution"].apply(utils.extract_screen)
laptops["Weight"] = laptops["Weight"].apply(utils.extract_weigth)

x_features = ['Cpu', 'Ram', 'ScreenResolution','Weight']
y_feature = 'Price_euros'

X = laptops.loc[:, x_features]
y = laptops[y_feature]

X_train = X.sample(frac=0.8, random_state=2021)
X_test = X.drop(X_train.index)

y_train = y[X_train.index]
y_test = y[X_test.index]

normalizer = keras.layers.experimental.preprocessing.Normalization()

normalizer.adapt(np.array(X_train))

model = keras.models.Sequential()
# Capa d'entrada
model.add(normalizer)
# Capes ocultes
model.add(tf.keras.layers.Dense(64, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(64, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(64, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(64, activation=tf.nn.relu))




# Capa de sortida
model.add(tf.keras.layers.Dense(1))  # regresio capa soritida 1

model.compile(optimizer=tf.optimizers.Adam(learning_rate=0.0001),
              loss="mean_squared_error",
              metrics=tfa.metrics.RSquare(y_shape=(1,)))

history = model.fit(X_train, y_train, epochs=1000, validation_split=0.2)

loss, r2 = model.evaluate(X_test, y_test)
print('r2: ',r2)

utils.plot_loss(history)

#dropout desactiva algunas neuronas
