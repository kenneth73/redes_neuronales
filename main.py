# clasificar las fotos, hacemos clasificacion
# https://www.youtube.com/watch?v=wQ8BIBpya2k
#https://www.kaggle.com/ajaypalsinghlo/covid-world-vaccination-progress-india
import tensorflow as tf
import matplotlib.pyplot as plt

#print (tf.__version__)

mnist = tf.keras.datasets.mnist

(x_train, y_train), (x_test, y_test) = mnist.load_data()

#para normalizar escalar los datos entre 0 y 1
x_train = tf.keras.utils.normalize(x_train, axis=1)
x_test = tf.keras.utils.normalize(x_test, axis=1)

# print(x_train[0])
# plt.imshow(x_train[0], cmap= plt.cm.binary)
# plt.show()

# crearemos el modelo
model = tf.keras.models.Sequential()
model.add(tf.keras.layers.Flatten()) # aplanar el modelo
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(10, activation=tf.nn.softmax))

model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])
model.fit(x_train, y_train, epochs=3)

val_loss, val_cc = model.evaluate(x_test, y_test)

print('val_loss',val_loss)
print('val_cc',val_cc)


#podemos guardar el modelo
model.save('epic_num_reader.model')
new_model = tf.keras.models.load_model('epic_num_reader.model')

#para realizar predicciones
predictions = new_model.predict([x_test])

#para visualizar los resultados
import numpy as np

print(np.argmax(predictions[0]))
plt.imshow(x_test[0])
plt.show()

#cambiar el loss para poder predecir diferentes tipos de salidas, clasificacion, regresion, etc..
